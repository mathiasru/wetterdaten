package com.itkolleg.wetterdaten.controller;

import com.itkolleg.wetterdaten.entity.Messwerte;
import com.itkolleg.wetterdaten.exception.MesswertNotFoundException;
import com.itkolleg.wetterdaten.service.MesswerteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@Controller
@RequestMapping("")
public class MesswertControllerGUI {

    @Autowired
    private MesswerteService messwerteService; //Dependency Injection

    @GetMapping("/list")
    public String showDepartmentList(Model model) {
        List<Messwerte> messwerteList = messwerteService.getAllMesswerte();
        model.addAttribute("messwerteList", messwerteList); //übergibt dem Model die Liste aller Users
        return "messwerte.html";
    }
    @GetMapping("/delete/{id}")
    public String deleteEmployee(@PathVariable("id") Long id, RedirectAttributes redirectAttributes) throws MesswertNotFoundException {
        messwerteService.deleteMesswertById(id); //wird über CrudRepository implementiert
        redirectAttributes.addFlashAttribute("message", "Messwert wurde gelöscht!"); //mitgeben einer Message für den Alert
        return "redirect:/list"; //zurück auf die Startseite
    }

    @GetMapping("/new")
    public String showNewForm(Model model) {
        model.addAttribute("messwert", new Messwerte()); //übergibt dem Model ein leeres User-Objekt
        model.addAttribute("pageTitle", "Add New Messwert"); //übergibt dem Model einen String für den Titel der Seite
        return "messwert_save.html";
    }

    @PostMapping("/save")
    public String saveMesswert(Messwerte messwerte, RedirectAttributes redirectAttributes) throws MesswertNotFoundException {
        messwerteService.saveMesswerte(messwerte); //wird über CrudRepository implementiert
        redirectAttributes.addFlashAttribute("message", "Messwert wurde gespeichert."); //mitgeben einer Message für den Alert
        return "redirect:/list"; //zurück auf die Startseite
    }
}
