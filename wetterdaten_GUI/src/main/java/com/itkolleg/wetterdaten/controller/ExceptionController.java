package com.itkolleg.wetterdaten.controller;

import com.itkolleg.wetterdaten.exception.ExceptionDto;
import com.itkolleg.wetterdaten.exception.MesswertNotFoundException;
import com.itkolleg.wetterdaten.exception.MesswertValidationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ExceptionController extends ResponseEntityExceptionHandler {

    //Validierungsgfehler ab 2000
    //Not-Found Fehler ab 1000

    @ExceptionHandler(MesswertNotFoundException.class)
    public ResponseEntity<ExceptionDto> messwertNotFoundException(MesswertNotFoundException messwertNotFoundException) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ExceptionDto("1000", messwertNotFoundException.getMessage()));
    }

    @ExceptionHandler(MesswertValidationException.class)
    public ResponseEntity<ExceptionDto> messwertValidationException(MesswertValidationException messwertValidationException) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ExceptionDto("2000", messwertValidationException.getMessage()));
    }

}
