package com.itkolleg.wetterdaten.service;

import com.itkolleg.wetterdaten.entity.Messwerte;
import com.itkolleg.wetterdaten.exception.MesswertNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MesswerteServiceImplementierung implements MesswerteService {

    @Autowired
    private DBZugriffMesswerte dbZugriffMesswerte;

    @Override
    public Messwerte saveMesswerte(Messwerte messwerte) {
        return dbZugriffMesswerte.saveMesswerte(messwerte);
    }

    @Override
    public List<Messwerte> getAllMesswerte() {
        return dbZugriffMesswerte.getAllMesswerte();
    }

    @Override
    public List<Messwerte> findAllByMesstation(String messtation) {
        return dbZugriffMesswerte.findAllByMesstation(messtation);
    }

    @Override
    public Messwerte getMesswertById(Long id) throws MesswertNotFoundException {
        return dbZugriffMesswerte.getMesswertById(id);
    }

    @Override
    public void deleteMesswertById(Long id) throws MesswertNotFoundException{
        dbZugriffMesswerte.deleteMesswertById(id);
    }
}
