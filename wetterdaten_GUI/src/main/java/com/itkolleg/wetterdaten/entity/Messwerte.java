package com.itkolleg.wetterdaten.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class Messwerte {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @NotNull(message = "Datenfeld darf nicht Leer sein!")
    private Double temperatur;

    @NotNull(message = "Datenfeld darf nicht Leer sein!")
    @Size(min = 2, message = "Minderstlaenge beachten!")
    private String messtation;

    public Messwerte(Double temperatur, String messtation) {
        this.temperatur = temperatur;
        this.messtation = messtation;
    }
}
