package com.itkolleg.wetterdaten.repository;

import com.itkolleg.wetterdaten.entity.Messwerte;
import com.itkolleg.wetterdaten.exception.MesswertNotFoundException;
import com.itkolleg.wetterdaten.service.DBZugriffMesswerte;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class MesswerteRepositoryJPAImplementierung implements DBZugriffMesswerte {

    @Autowired
    private MesswerteRepositoryJPA messwerteRepositoryJPA;

    @Override
    public Messwerte saveMesswerte(Messwerte messwerte) {
        return messwerteRepositoryJPA.save(messwerte);
    }

    @Override
    public List<Messwerte> getAllMesswerte() {
        return messwerteRepositoryJPA.findAll();
    }

    @Override
    public List<Messwerte> findAllByMesstation(String messtation) {
        return messwerteRepositoryJPA.findAllByMesstationIgnoreCase(messtation);
    }

    @Override
    public Messwerte getMesswertById(Long id) throws MesswertNotFoundException {
        Optional<Messwerte> messwertDB = messwerteRepositoryJPA.findById(id);
        if (messwertDB.isPresent()){
            return messwertDB.get();
        } else {
            throw new MesswertNotFoundException("Kein Messwert mit der " + id + " gefunden!");
        }
    }

    @Override
    public void deleteMesswertById(Long id) throws MesswertNotFoundException {
        Optional<Messwerte> messwertDB = messwerteRepositoryJPA.findById(id);
        if (messwertDB.isPresent()){
            messwerteRepositoryJPA.delete(messwertDB.get());
        } else {
            throw new MesswertNotFoundException("Kein Messwert mit der " + id + " gefunden!");
        }
    }
}
