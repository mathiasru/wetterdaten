package com.itkolleg.wetterdaten.repository;

import com.itkolleg.wetterdaten.entity.Messwerte;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MesswerteRepositoryJPA extends JpaRepository<Messwerte, Long> {
    List<Messwerte> findAllByMesstationIgnoreCase(String messtation);
}
