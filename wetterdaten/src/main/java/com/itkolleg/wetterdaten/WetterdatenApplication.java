package com.itkolleg.wetterdaten;

import com.itkolleg.wetterdaten.entity.Messwerte;
import com.itkolleg.wetterdaten.service.MesswerteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WetterdatenApplication implements ApplicationRunner {

	@Autowired
	private MesswerteService messwerteService;

	public static void main(String[] args) {
		SpringApplication.run(WetterdatenApplication.class, args);
	}

	@Override
	public void run(ApplicationArguments args) throws Exception {
		//Beim Starten der Applikation
		messwerteService.saveMesswerte(new Messwerte(21.2, "Landeck"));
		messwerteService.saveMesswerte(new Messwerte(22.2, "Landeck"));
		messwerteService.saveMesswerte(new Messwerte(23.2, "Landeck"));
		messwerteService.saveMesswerte(new Messwerte(21.2, "Imst"));
		messwerteService.saveMesswerte(new Messwerte(22.2, "Imst"));
		messwerteService.saveMesswerte(new Messwerte(23.2, "Imst"));
		messwerteService.saveMesswerte(new Messwerte(21.2, "Innsbruck"));
		messwerteService.saveMesswerte(new Messwerte(22.2, "Innsbruck"));
		messwerteService.saveMesswerte(new Messwerte(23.2, "Innsbruck"));

	}
}
