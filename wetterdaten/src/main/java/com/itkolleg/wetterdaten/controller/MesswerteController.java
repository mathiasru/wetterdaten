package com.itkolleg.wetterdaten.controller;

import com.itkolleg.wetterdaten.entity.Messwerte;
import com.itkolleg.wetterdaten.exception.MesswertNotFoundException;
import com.itkolleg.wetterdaten.exception.MesswertValidationException;
import com.itkolleg.wetterdaten.service.MesswerteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v1/messwerte")
public class MesswerteController {

    @Autowired
    private MesswerteService messwerteService; //Dependency Injection

    @PostMapping("")
    public ResponseEntity<Messwerte> saveMesswerte(@Valid @RequestBody Messwerte messwerte, BindingResult bindingResult) throws MesswertValidationException {
        String errorMesssage = "Validierungsfehler: ";
        if (bindingResult.hasErrors()) {
            for (ObjectError error : bindingResult.getAllErrors()) {
                errorMesssage += error.getObjectName() + " Error: " + error.getDefaultMessage() + "; ";
            }
            throw new MesswertValidationException(errorMesssage);
        } else {
            return ResponseEntity.status(HttpStatus.OK).body(messwerteService.saveMesswerte(messwerte));
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<Messwerte> getMesswerteById(@PathVariable Long id) throws MesswertNotFoundException {
        return ResponseEntity.status(HttpStatus.OK).body(messwerteService.getMesswertById(id));

    }

    @GetMapping("")
    public ResponseEntity<List<Messwerte>> getAllMesswerte() {
        return ResponseEntity.status(HttpStatus.OK).body(messwerteService.getAllMesswerte());
    }

    @GetMapping("station/{messtation}")
    public ResponseEntity<List<Messwerte>> getAllMesswerteByMesstation(@PathVariable("messtation") String messtation) {
        return ResponseEntity.status(HttpStatus.OK).body(messwerteService.findAllByMesstation(messtation));
    }

    @DeleteMapping("/{id}")
    public String deleteMesswerte(@PathVariable("id") Long id) throws MesswertNotFoundException {
        this.messwerteService.deleteMesswertById(id);
        return "Messwert wurde gelöscht";
    }


}
