package com.itkolleg.wetterdaten.service;

import com.itkolleg.wetterdaten.entity.Messwerte;
import com.itkolleg.wetterdaten.exception.MesswertNotFoundException;

import java.util.List;

public interface MesswerteService {

    Messwerte saveMesswerte(Messwerte messwerte);

    List<Messwerte> getAllMesswerte();

    List<Messwerte> findAllByMesstation(String messtation);

    Messwerte getMesswertById(Long id) throws MesswertNotFoundException;

    void deleteMesswertById(Long id) throws MesswertNotFoundException;

}
